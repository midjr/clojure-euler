;; problem 2 - Even Fibonacci numbers

(def fib (lazy-cat 
               [0 1]
               (map + fib (rest fib))))

(apply + (take-while (partial >= 4000000)(filter even? fib)))
