;; problem 1 - Multiples of 3 and 5

(defn- multiple_of?
  "Returns true if [num] is divisible by [div]"
  [div num]
  (= (mod num div) 0))

(defn sum_of_multiples_from
  "Returns sum of all numbers divisible by 3,5 from 1 to [start] exclusive"
  ([start] (sum_of_multiples_from (dec start) 0))
  ([start total] 
   (if (<= start 0) 
     total
     (recur (dec start) (if 
                            (or (multiple_of? 3 start) (multiple_of? 5 start))
                          (+ start total)
                          total)))))

(defn alternate_sum
  "Another solution to sum_of_multiples_from"
  ([start]
   (apply + (filter #(or (multiple_of? 3 %) (multiple_of? 5 %)) (range 1 start)))))
